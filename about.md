---
layout: "page"
title: About MDEF
icon: fa-user
order: 2
---


The aim of this master is to provide the strategic vision and tools for designers,
  sociologists, economists and computer scientists, to become agents of change in multiple
  professional environments. The programme focuses on the design of interventions in the form of products,
  platforms and deployments that aim to produce new emergent futures by previously analysing the current
  challenges in society and industry.
