---
title: Interesting places
layout: "page"
icon: fa-map-marked-alt
order: 5
---


# Here we have some interesting places in Poblenou. Add your favorite places!
<iframe src="https://www.google.com/maps/d/u/0/embed?mid=1p3kmsJjecqJ_tUwINWj1fVaLdsPIJIiI" width="840" height="680"></iframe>
