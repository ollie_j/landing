---
title: Navigating Uncertainty
layout: "page"
order: 05
---
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/afe5385dc9c5-1200px_Suzhou_star_cartography_S.jpg)  
[Chinese costellations](https://en.wikipedia.org/wiki/Chinese_constellations)    

### Faculty
Tomas Diez  
Oscar Tomico   
Jose Luis de Vicente  
Pau Alsina  
Indy Johar  

### Assistant
Mariana Quintero  

### Syllabus and Learning Objectives
According to experts, we lost the battle with climate change between 1979 and 1989. Even a decade before, the Club of Rome was already aware of the systemic problems of the model of development of the global economy, and in the voracity of consumption. Designers have been voices that always brought optimism to the discussion, beyond politicians. In 2004 designer Bruce Mau produced an exhibition and book on the topic “Massive Change”, where seemed to be a glimpse of hope based on the new perspective of starting a century and a millennium. Almost 15 years later, nations got together to declare the war on climate change, the Paris Declaration took the covers of main media around the world. Nothing seems to change: fracking is a practice that is being increased, the poles are melting, and we are extinguishing animal species at a rate never seen before. 

This course is intended to help students framing research questions in an age of massive change and constant uncertainty. The sessions organized with experts are aimed to give students the framework to understand the resistance to change in society and find ways to overcome them. 

During Week 5 we want to review the world crisis, from the perspective of experts that have produced work on climate change, the relationship between humans and technology, or just about the definition of humans. Students will be encouraged to find the new glimpses of hope in the current state of affairs in the world and will be guided to define their space of intervention. The course will be organized in sessions led by the Research Studio and MDEF directors. 

### Presentations from Tutors
Pau Alsina: Unfolding the Political Capacities of Design 
<iframe src="https://drive.google.com/file/d/1Jq4WY-bFRILzqjVOzbdsLGo6bDm2yZeC/preview" width="700" height="450"></iframe>

Mariana Quintero: Read-Write Technologies   
<iframe src="https://drive.google.com/file/d/1M2oo7MiVNu7ifcVbY4kHsF7wVWH18yRd/preview" frameborder="0" width="700" height="450" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


### Total Duration
Classes: 3-6 p.m, Mon-Fri (15 per week)  
Student work hours: 20 per week  
Total hours per student: 35 hours  

### Structure and Phases
* *Monday 29th*       
Conversation with Jose Luis de Vicente    
* *Tuesday 30th*       
Conversation with Pau Alsina   
* *Wednesday 31th*       
Conversation with Indy Johar  
* *Monday 5th*       
Open discussion, and debate. Presentations   
 

### Output
Create and document in your site your own reading list. Elaborate a literature review, State of the Art related to projects in your field of interest. Positioning in relation to a subject of your interest.

### Grading Method
Presentation 30%  
Documentation 50%  
Assistance and participation in conversations 20%   

### Bibliography
To be updated online

### Background Research Material
To be updated online

### Requirements for the Students
Be open for philosophical debates during the sessions, and to document your thinking process and argument it.

### Infrastructure Needs
Just the classroom

### TOMAS DIEZ
![image](https://gitlab.com/MDEF/landing/raw/master/assets/images/0302393192f2-Screenshot_2018_09_10_at_12.37.01_20180912191624503.jpg)

### Email Address
<tomasdiez@iaac.net>

### Personal Website
[Tomas Diez](http://tomasdiez.com)

### Twitter Account
@tomasdiez

### Facebook Profile
tomasdiez77

### Professional BIO
Tomas Diez is a Venezuelan Urbanist specialized in digital fabrication and its implications on the future cities and society. He is the co-founder of Fab Lab Barcelona, leads the Fab City Research Laboratory, and is a founding partner of the Fab City Global Initiative. He is the director of the Master in Design for Emergent Futures at the Institute for Advanced Architecture of Catalonia (IAAC) in Barcelona, where he is faculty in urban design and digital fabrication. Tomas is co-founder of other initiatives such as Smart Citizen (open source tools for citizen engagement), Fab City (locally productive, globally connected cities), Fablabs.io (the listing of fab labs in the world), and StudioP52 (art and design space in Barcelona).