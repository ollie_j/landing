---
title: Term 1
icon: fa-th
order: 3
---

{% for course in site.seminar1 %}
 [{{course.title}}]({{course.url|relative_url}})
{% endfor %}
